"""
Some utils
"""

def interpolate(x, x0, x1, y0, y1):
    """Calculate the linear interpolated value."""
    y = y0 + (y1 - y0) * (x - x0) / (x1 - x0)
    return y

