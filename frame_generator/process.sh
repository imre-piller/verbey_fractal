#!/bin/bash

# Try to create directories (for the first start)
mkdir frames
mkdir params

rm params/*
python3 generator.py
for fn in $(ls params); do
    echo $fn >> progress_times.txt
    date >> progress_times.txt
    ./mandelbulber -format png -o image_width=1024 -o image_height=768 params/$fn > /dev/null
    date >> progress_times.txt
    echo $fn
done

