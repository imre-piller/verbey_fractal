import os

from utils import interpolate
import keyframes


def load_params(fract_file_path):
    """Load the parameters from the fractal settings file."""
    params = {}
    with open(fract_file_path) as fract_file:
        for line in fract_file:
            splitted = line.split(' ')
            if len(splitted) > 1:
                name = splitted[0]
                value = ' '.join(splitted[1:])
                params[name] = value[:-2]
    return params


def calc_view_position(t, key_positions):
    """
    Calculate the view position according to the time and key positions.
    :param t: time as an integer in milliseconds
    :param key_positions: dictionary of positions
    :returns: a dictionary with position and angle informations
    """
    position = {}
    field_names = [
        'view_point_x', 'view_point_y', 'view_point_z',
        'angle_alfa', 'angle_beta', 'angle_gamma'
    ]
    times = sorted(key_positions.keys())
    for i in range(len(times) - 1):
        if t < times[i + 1]:
            a = times[i]
            b = times[i + 1]
            for index, field_name in enumerate(field_names):
                first_value = key_positions[times[i]][index]
                last_value = key_positions[times[i + 1]][index]
                position[field_name] = interpolate(t, a, b, first_value, last_value)
            return position
    raise ValueError('Invalid t value! {}'.format(t))


def calc_parameter_value(t, keyframe_values):
    """
    Calculate the value of the
    :param t: time as an integer in milliseconds
    :param keyframe_values: the dictionary of the values
    :return: a floating point value
    """
    times = sorted(keyframe_values.keys())
    for i in range(len(times) - 1):
        if t < times[i + 1]:
            a = times[i]
            b = times[i + 1]
            first_value = keyframe_values[times[i]]
            last_value = keyframe_values[times[i + 1]]
            return interpolate(t, a, b, first_value, last_value)
    raise ValueError('Invalid t value! {}'.format(t))


def calc_section_1_params(t):
    """Calculate the parameters of the first section."""
    params = load_params('init_params/section_1_base.fract')
    params['file_destination'] += '{}/frames/{:08d}.png'.format(os.getcwd(), t)
    params['power'] = calc_parameter_value(t, keyframes.section_1_powers)
    position = calc_view_position(t, keyframes.section_1_positions)
    params.update(position)
    if t < 25000:
        params['view_point_y'] = -195 * (1 - (t / 25000)) ** 4 - 5
    return params


def calc_section_2_params(t):
    """Calculate the parameters of the second section."""
    params = load_params('init_params/section_2_base.fract')
    params['file_destination'] = '{}/frames/{:08d}.png'.format(os.getcwd(), t)
    position = calc_view_position(t, keyframes.section_2_positions)
    params.update(position)
    return params


def calc_section_3_params(t):
    """Calculate the parameters of the third section."""
    params = load_params('init_params/section_3_base.fract')
    params['file_destination'] = '{}/frames/{:08d}.png'.format(os.getcwd(), t)
    params['fractal_constant_factor'] = calc_parameter_value(t, keyframes.section_3_factors)
    position = calc_view_position(t, keyframes.section_3_positions)
    params.update(position)
    return params


def calc_section_4_params(t):
    """Calculate the parameters of the third section."""
    params = load_params('init_params/section_4_base.fract')
    params['file_destination'] = '{}/frames/{:08d}.png'.format(os.getcwd(), t)
    position = calc_view_position(t, keyframes.section_4_positions)
    params.update(position)
    return params


def calc_section_5_params(t):
    """Calculate the parameters of the third section."""
    params = load_params('init_params/section_1_base.fract')
    params['file_destination'] = '{}/frames/{:08d}.png'.format(os.getcwd(), t)
    params['fractal_constant_factor'] = calc_parameter_value(t, keyframes.section_5_factors)
    position = calc_view_position(t, keyframes.section_5_positions)
    params.update(position)
    return params


def calc_params(t):
    """
    Calculate the parameters of the fractal at the given frame.
    :param t: the time parameter (integer value in milliseconds)
    :returns: a dictionary with the parameters of the fractal.
    """
    params = {}
    if t < 0:
        raise ValueError('Invalid time parameter! {}'.format(t))
    if t < 196000:
        params = calc_section_1_params(t)
    elif t < 258000:
        params = calc_section_2_params(t)
    elif t < 288000:
        params = calc_section_3_params(t)
    elif t < 344000:
        params = calc_section_4_params(t)
    else:
        params = calc_section_5_params(t)
    return params


def save_params(params, fract_file_path):
    """
    Save the parameters to the given fractal settings file.
    :param params: a dictionary which contains the parameters of the fractal
    :param fract_file_path: the path of the .fract file
    """
    with open(fract_file_path, 'w') as fract_file:
        for name, value in params.items():
            fract_file.write(name)
            fract_file.write(' ')
            fract_file.write(str(value))
            fract_file.write(';\n')


if __name__ == '__main__':
    for t in range(0, 480000, 100):
        params = calc_params(t)
        save_params(params, 'params/{:08d}.fract'.format(t))
