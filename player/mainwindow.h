#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QElapsedTimer>
#include <QMainWindow>
#include <QMap>
#include <QTimer>
#include <QVector>

class Stop
{
public:

    Stop();
    Stop(int time, char trigger);

    int time;
    char trigger;
};

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    /**
     * @brief Mouse wheel event
     * @param event
     */
    virtual void wheelEvent(QWheelEvent *event);

    /**
     * @brief Keyboard event handler
     * @param event
     */
    virtual void keyPressEvent(QKeyEvent *event);

    /**
     * @brief Keyboard release event handler
     * @param event
     */
    virtual void keyReleaseEvent(QKeyEvent *event);

public slots:

    /**
     * @brief Timer event for changing the frames.
     */
    void timerEvent();

protected:

    /**
     * @brief Calculate the index of the actual frame.
     */
    int calcActualFrameIndex();

    /**
     * @brief Display the given frame.
     * @param frameIndex
     */
    void displayFrame(int frameIndex);

private:

    /**
     * @brief Collect the indices of the frames from the given directory.
     * @return the sorted list of frame indices
     */
    QVector<int> collectFrameIndices(const QString& path);

    /**
     * @brief Load the stop times for synchronization.
     */
    void loadStops();

    /**
     * @brief The vector of the frame incides
     */
    QVector<int> _frameIndices;

    /**
     * @brief Timer object
     */
    QTimer* _timer;

    /**
     * @brief Calculate the elapsed time from the start.
     */
    QElapsedTimer* _elapsedTimer;

    /**
     * @brief The index of the displayed frame.
     */
    int _displayedFrameIndex;

    /**
     * @brief the difference between the current and the expected time in milliseconds.
     */
    int _deltaTime;

    /**
     * @brief The time in milliseconds when the animation should stop.
     */
    int _stopTime;

    /**
     * @brief Is it the flash back image?
     */
    bool _isFlashBack;

    /**
     * @brief Stop times for synchronization
     */
    QMap<int, Stop> _stops;

    /**
     * @brief ui
     */
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
