#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>
#include <QDir>
#include <QKeyEvent>
#include <QPixmap>
#include <QWheelEvent>

#include <QtAlgorithms>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    _frameIndices = collectFrameIndices("frames");
    _displayedFrameIndex = -1;
    _timer = new QTimer();
    connect(_timer, &QTimer::timeout, this, &MainWindow::timerEvent);
    _timer->setInterval(40);
    _elapsedTimer = new QElapsedTimer();
    _elapsedTimer->start();
    _timer->start();

    loadStops();

    _deltaTime = 0;
    _stopTime = 0;

    _isFlashBack = false;

    ui->setupUi(this);
    this->setStyleSheet("background-color: black;");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::wheelEvent(QWheelEvent* event)
{
    if (event->delta() < 0) {
        _deltaTime -= 40;
    }
    else {
        _deltaTime += 100;
    }
}

void MainWindow::keyPressEvent(QKeyEvent* event)
{
    if (event->key() + 32 == _stops[_stopTime].trigger) {
        _deltaTime = _stopTime - _elapsedTimer->elapsed();
        _stopTime = _stops[_stopTime].time;
        qDebug() << "Ok " << _stopTime;
        qDebug() << "/\\" << _deltaTime;
    }

    if (event->key() + 32 == 'k') {
        _isFlashBack = true;
    }
    // qDebug() << event->key();
}

void MainWindow::keyReleaseEvent(QKeyEvent* event)
{
    _isFlashBack = false;
}

void MainWindow::timerEvent()
{
    int actualFrameIndex = calcActualFrameIndex();
    if (actualFrameIndex != _displayedFrameIndex) {
        displayFrame(actualFrameIndex);
        _displayedFrameIndex = actualFrameIndex;
    }
}

int MainWindow::calcActualFrameIndex()
{
    if (_isFlashBack) {
        // return 310000;
        return 305000;
    }

    int elapsedTime = _elapsedTimer->elapsed() + _deltaTime;
    int requiredIndex = elapsedTime;
    if (requiredIndex > _stopTime) {
        return _stopTime;
    }

    // TODO: Some optimization required!!!
    for (int index : _frameIndices) {
        if (index >= requiredIndex) {
            return index;
        }
    }
    return 0;
}

void MainWindow::displayFrame(int frameIndex)
{
    QString path = QString("frames/%1.png").arg(frameIndex, 8, 10, QChar('0'));
    QPixmap pixmap(path);
    ui->frameDisplay->setPixmap(pixmap);
    // qDebug() << frameIndex;
}

QVector<int> MainWindow::collectFrameIndices(const QString &path)
{
    qDebug() << "Collect frame indices ...";
    QVector<int> frameIndices;
    QDir directory(path);
    QStringList fileNames = directory.entryList();
    for (const QString& fileName : fileNames) {
        // WARN: The extension of the image files should match!
        if (fileName.endsWith("png")) {
            QString numericPart = fileName.mid(0, 8);
            int index = numericPart.toInt();
            frameIndices.append(index);
        }
    }
    qSort(frameIndices);
    qDebug() << "[OK]";
    return frameIndices;
}

void MainWindow::loadStops()
{
    _stops[0] = Stop(25000, 'q');
    _stops[25000] = Stop(44000, 'w');
    _stops[44000] = Stop(69000, 'e');
    _stops[69000] = Stop(85000, 'r');
    _stops[85000] = Stop(115000, 'a');
    _stops[115000] = Stop(132000, 's');
    _stops[132000] = Stop(157000, 'd');
    _stops[157000] = Stop(173000, 'f');
    _stops[173000] = Stop(187000, 'x');
    _stops[187000] = Stop(195960, 'c');
    _stops[195960] = Stop(203000, 'q');
    _stops[203000] = Stop(208000, 'w');
    _stops[208000] = Stop(215000, 'e');
    _stops[215000] = Stop(220000, 'r');
    _stops[220000] = Stop(225000, 'a');
    _stops[225000] = Stop(230000, 's');
    _stops[230000] = Stop(235000, 'd');
    _stops[235000] = Stop(240000, 'f');
    _stops[240000] = Stop(245000, 'x');
    _stops[245000] = Stop(250000, 'c');
    _stops[250000] = Stop(257960, 'v');
    _stops[257960] = Stop(273000, 'q');
    _stops[273000] = Stop(288000, 'w');
    _stops[288000] = Stop(296000, 'a');
    _stops[296000] = Stop(306000, 's');
    _stops[306000] = Stop(320000, 'd');
    _stops[320000] = Stop(328000, 'f');
    _stops[328000] = Stop(343960, 'g');
    _stops[343960] = Stop(397000, 'q');
    _stops[397000] = Stop(400000, 'w');
    _stops[400000] = Stop(402000, 'e');
    _stops[402000] = Stop(407000, 'r');
    _stops[407000] = Stop(410000, 'a');
    _stops[410000] = Stop(412000, 's');
    _stops[412000] = Stop(422000, 'd');
    _stops[422000] = Stop(424000, 'f');
    _stops[424000] = Stop(425000, 'y');
    _stops[425000] = Stop(426000, 'x');
    _stops[426000] = Stop(427000, 'c');
    _stops[427000] = Stop(428000, 'v');
    _stops[428000] = Stop(429000, 'a');
    _stops[429000] = Stop(430000, 's');
    _stops[430000] = Stop(431000, 'd');
    _stops[431000] = Stop(433000, 'f');
    _stops[433000] = Stop(434000, 'y');
    _stops[434000] = Stop(435000, 'x');
    _stops[435000] = Stop(436000, 'c');
    _stops[436000] = Stop(437000, 'v');
    _stops[437000] = Stop(438000, 'a');
    _stops[438000] = Stop(448000, 's');
    _stops[448000] = Stop(452000, 'd');
    _stops[452000] = Stop(470000, 'f');
    _stops[470000] = Stop(480000, 'x');
    _stops[480000] = Stop(480000, 'c');
}

Stop::Stop()
{
    this->time = 0;
    this->trigger = '0';
}

Stop::Stop(int time, char trigger)
{
    this->time = time;
    this->trigger = trigger;
}
