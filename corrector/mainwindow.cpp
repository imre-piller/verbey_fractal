#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>
#include <QImage>
#include <QPainter>
#include <QPixmap>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    fading();

    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::szorny()
{
    QPixmap* background = new QPixmap("/tmp/background.png");
    QPixmap* szorny = new QPixmap("/tmp/image.png");
    QPainter* painter = new QPainter(background);
    painter->setOpacity(0.5);
    painter->drawImage(QRect(100, 100, 400, 300), szorny->toImage());
    background->save("/tmp/result.png");
}

void MainWindow::black()
{
    QPixmap* background = new QPixmap("/tmp/sample.png");
    QPainter* painter = new QPainter(background);
    painter->setOpacity(0.8);
    painter->setBrush(Qt::black);
    painter->drawRect(QRect(100, 100, 400, 300));
    background->save("/tmp/result.png");
}

void MainWindow::fading()
{
    // for (int index = 0; index < 8000; index += 40) {
    for (int index = 477000; index < 480000; index += 40) {
        qDebug() << index;
        double opacity = 1.0 - (double)(480000 - index) / 3000;
        QString path = QString("/home/imre/frames/frames/%1.png").arg(index, 8, 10, QChar('0'));
        QPixmap* background = new QPixmap(path);
        QPainter* painter = new QPainter(background);
        painter->setOpacity(opacity);
        painter->setBrush(Qt::black);
        painter->drawRect(QRect(0, 0, 1024, 768));
        background->save(path);
    }
}
